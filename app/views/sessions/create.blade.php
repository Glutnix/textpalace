@extends('layouts.master')

@section('content')

	{{-- var_dump( Session::all() ) --}}

	<div class="container">
		<h1>Log In</h1>
		
		{{ Form::open(array(
			'route' => 'login.store',
		)) }}
			<div class="form-group">
				{{ Form::label('username', 'Username') }}
				{{ Form::text('username', NULL, array(
					'placeholder' => 'username',
					'class' => 'form-control'
				)) }}
				@if ($errors->get('username'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('username') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password', array(
					'placeholder' => 'password',
					'class' => 'form-control'
				)) }}
				@if ($errors->get('password'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('password') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>

			<div class="checkbox">
				{{ Form::label('remember_me', "Keep me logged in for five years") }}
				{{ Form::checkbox('remember_me', true, false) }}
			</div>


			<div class="form-group">
				{{ Form::submit('Log In', array(
					'class' => 'btn btn-primary'
				)) }}
			</div>
		{{ Form::close() }}

	</div>

@stop