<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">

        <script src="{{ asset('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">Text Palace</a>
            </div>
            <div class="navbar-collapse collapse">

              <ul class="nav navbar-nav">
                <li><a href="/users/">Users</a></li>
                <li><a href="/entries/">Entries</a></li>
              </ul>

              @if( Auth::check() )
                <div class="navbar-form navbar-right">
                    {{ link_to_route('logout', "Logout", NULL, array(
                      'class' => 'btn btn-default'
                    )) }}
                </div>
                <ul class="nav navbar-nav navbar-right">
                  <li>
                    {{ Gravatar::image(Auth::user()->email, NULL, (array(
                      'width' => 30, 
                      'style' => 'padding-top: 10px'
                    ))) }}
                  </li>
                  <li>
                    {{ link_to_route('users.username', Auth::user()->username, Auth::user()->username) }}
                  </li>
                </ul>
              @else

                @if( Route::currentRouteName() !== 'login.create' )

                  {{ Form::open(
                    array(
                      'route' => 'login.create',
                      'class' => 'navbar-form navbar-right',
                      'role' => 'form'
                    )
                  ) }}
                    <div class="form-group">
                      {{ Form::text('username', NULL, array(
                        'class' => 'form-control',
                        'placeholder' => 'Username'
                      )) }}
                    </div>
                    <div class="form-group">
                      {{ Form::password('password', array(
                        'class' => 'form-control',
                        'placeholder' => 'Password'
                      )) }}
                    </div>
                    {{ Form::submit('Login', array('class' => 'btn btn-success'))}}
                  {{ Form::close() }}
                
                @endif
              @endif
            </div><!--/.navbar-collapse -->
          </div>
        </div>

        @if( Session::has('flash-page-danger'))
          <div class="container">
            <div class="alert alert-danger" role="alert">
             {{ Session::get('flash-page-danger') }}
            </div>
          </div>
        @endif

        @if( Session::has('flash-page-info'))
          <div class="container">
            <div class="alert alert-info" role="alert">
             {{ Session::get('flash-page-info') }}
            </div>
          </div>
        @endif
        
        @yield('content')

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="' + "{{ asset('js/vendor/jquery-1.11.0.min.js') }}" + '"><\/script>')</script>

        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

        <script src="{{ asset('js/main.js') }}"></script>

        @section('footer-js')
        @show
    </body>
</html>
