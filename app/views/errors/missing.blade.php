@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="jumbotron">
					<h1>404 Not Found</h1>
					<p>Sorry that this still kinda feels like a U2 song.</p>
				</div>
			</div>
		</div>
	</div>
@stop