@extends('layouts.master')

@section('content')

	<div class="container">
		<h1>Update Entry</h1>
		
		{{ Form::model( $entry, array(
			'route' => array('entries.update', $entry->id),
			'method' => 'put'
		)) }}
			<div class="form-group">
				{{ Form::label('title', 'Title') }}
				{{ Form::text('title', NULL, array(
					'placeholder' => 'Text Title',
					'class' => 'form-control'
				)) }}
				@if ($errors->get('title'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('title') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>
			
			<div class="form-group">
				{{ Form::label('text', 'Text') }}
				<div class="markdowneditor"></div>
				{{ Form::textarea('text', NULL, array(
					'placeholder' => 'Your Text Asplode',
					'class' => 'form-control'
				)) }}
				
				@if ($errors->get('text'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('text') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>

			<div class="form-group">
				{{ Form::submit('Update', array(
					'class' => 'btn btn-primary'
				)) }}
			</div>
		{{ Form::close() }}

	</div>

@stop

@section('footer-js')
	<script type="text/javascript" src="{{ asset('js/vendor/epiceditor/js/epiceditor.js') }}"></script>	
	<script type="text/javascript">
	var epicEditorOpts = {
		container: $('.markdowneditor').get(0), // where to put it
		textarea: 'text', // id of text area
		basePath: '/js/vendor/epiceditor', // where the themes folder is
		autogrow: true
	};
	var editor = new EpicEditor(epicEditorOpts).load();
	$('#text').hide();
	</script>
@stop



