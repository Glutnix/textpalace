@extends('layouts.master')

@section('content')

	<div class="container">
		<h1>Create Entry</h1>
		
		{{ Form::open(array(
			'route' => 'entries.store',
		)) }}
			<div class="form-group">
				{{ Form::label('title', 'Title') }}
				{{ Form::text('title', NULL, array(
					'placeholder' => 'Text Title',
					'class' => 'form-control'
				)) }}
				@if ($errors->get('title'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('title') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>
			
			<div class="form-group">
				{{ Form::label('text', 'Text') }}
				{{ Form::textarea('text', NULL, array(
					'placeholder' => 'Your Text Asplode',
					'class' => 'form-control'
				)) }}
				@if ($errors->get('text'))
					<span class="help-block">
						<ul>
							@foreach( $errors->get('text') as $error )
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</span>
				@endif
			</div>

			<div class="form-group">
				{{ Form::submit('Create', array(
					'class' => 'btn btn-primary'
				)) }}
			</div>
		{{ Form::close() }}

	</div>

@stop