@extends('layouts.master')

@section('content')

	<div class="container">
		<h1>{{{ $entry->title }}}</h1>
		
		<p>{{ Markdown::render($entry->text) }}</p>

		<p>Created by {{ $entry->user->username }} on {{ date('j F Y', time($entry->created_at)) }}</p>

		@if (Auth::check() && Auth::id() == $entry->user_id)
			{{ Form::open(
				array(
					'route' => array('entries.destroy', $entry->id ), 
					'method' => 'delete',
				)
			) }}
				
				{{ link_to_route('entries.edit', 'Edit', $entry->id,
					array('class' => 'btn btn-default')
				) }}

				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
			{{ Form::close() }}
		@endif

	</div>

@stop