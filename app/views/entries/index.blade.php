@extends('layouts.master')

@section('content')
<div class="container">
  <h1>Text Entries</h1>

  @if (Auth::check())
    <div>
      <a href="{{ route('entries.create') }}" class="btn btn-primary">
        <span class="glyphicon glyphicon-plus"></span> Create New Entry
      </a>
    </div>
  @endif

  <ul>
    @foreach($entries as $entry)
    <li>{{ link_to_route(
    'entries.show',                                                         // name of the route 
      $entry->id . " - " . $entry->title . " by " . $entry->user->username, // text to display
      array('id' => $entry->id)                                             // route parameters
      ) }}</li>
    @endforeach
  </ul>
  
</div>
@stop