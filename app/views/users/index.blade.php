@extends('layouts.master')

@section('content')
	<div class="container">
		<h1>Users</h1>
		<ul>
			@foreach($users as $user)
				<li>{{ link_to_route(
						'users.username', 
						$user->username, 
						array('username' => $user->username) 
					) }}</li>
			@endforeach
		</ul>
	</div>
@stop