<?php

class UserTableSeeder extends Seeder {
	
	public function run ()
	{
		DB::table('users')->truncate();

		User::create(array(
			'username'  => 'alice', 
			'password'  => Hash::make('alice'),
			'email' 	=> 'alice@example.com'
		));
		User::create(array(
			'username'  => 'bob', 
			'password'  => Hash::make('bob'),
			'email' 	=> 'bob@example.com'
		));
		User::create(array(
			'username'  => 'charles', 
			'password'  => Hash::make('charles'),
			'email' 	=> 'charles@example.com'
		));
		User::create(array(
			'username'  => 'david', 
			'password'  => Hash::make('david'),
			'email' 	=> 'david@example.com'
		));
		User::create(array(
			'username'  => 'elaine', 
			'password'  => Hash::make('elaine'),
			'email' 	=> 'elaine@example.com'
		));
		User::create(array(
			'username'  => 'francis', 
			'password'  => Hash::make('francis'),
			'email' 	=> 'francis@example.com'
		));
	}

}