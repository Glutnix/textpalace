<?php

function redirectNotOwner() {
	Session::reflash();
	Session::flash('flash-page-danger', "You don't own that." );
	return Redirect::home();
}