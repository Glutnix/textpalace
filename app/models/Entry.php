<?php

class Entry extends Eloquent {
	
	protected $fillable = array('title', 'text');

	public function user() {
		return $this->belongsTo('User');
	}

}