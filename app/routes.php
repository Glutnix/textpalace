<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', function() {
	return View::make('home');
}));

Route::get('/textpalace/about', function()
{
	return View::make('about');
});

Route::get('users', function () 
{
	$users = User::all();
	return View::make('users.index')->withUsers($users);
});


Route::get('users/{id}', function ($id)
{
	$user = User::findOrFail($id);

	return View::make('users.show')->withUser($user);

})->where('id', '[0-9]+');


Route::get('users/{username}', array( 
	'as' => 'users.username', 
	function ($username)
	{
		$user = User::where('username', $username)->firstOrFail();
		
		return View::make('users.show')->withUser($user);
	}
))->where('username', '[0-9A-Za-z\-]+');


Route::resource('entries', 'EntryController');


// *** SESSIONS ***

Route::get('login', array('as' => 'login.create', function() {
	return View::make('sessions.create');
}));

Route::post('login', array('as' => 'login.store', function() {
	if (!Auth::attempt(Input::only('username', 'password'), Input::get('remember_me'))) {
		return Redirect::route('login.create')->withErrors(array(
			'username' => 'Username and/or Password incorrect.'
		))->withInput();
	}
	Session::flash('flash-page-info', 'Welcome! You are now logged in as <strong>'. Auth::user()->username . "</strong>");
	return Redirect::intended();
}));

Route::get('logout', array('as' => 'logout', function() {
	Auth::logout();
	Session::flash('flash-page-info', 'You are now logged out.');
	return Redirect::back();
}));




// *** ERRORS ***

App::error(function(ModelNotFoundException $e)
{
    return Response::view('errors.missing', array(), 404);
});

App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});



