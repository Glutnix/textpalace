<?php

class EntryController extends \BaseController {

	public function __construct() {
		$this->beforeFilter('auth', array('except' => array('index', 'show') ));
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$entries = Entry::with('user')->get(); // Eager load the users
		return View::make('entries.index')->withEntries($entries);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('entries.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'title' => array('required', 'min:5', 'max:255'),
			'text' 	=> array('required', 'min:15')
		);

		$validator = Validator::make(
			Input::all(),
			$rules
		);

		if ($validator->fails()) {
			return Redirect::route('entries.create')->withErrors($validator)->withInput();
		}

		$entry = new Entry(Input::all());
		$entry->user_id = Auth::id();
		$entry->save();

		return Redirect::route('entries.show', array('id' => $entry->id));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$entry = Entry::findOrFail($id);
		return View::make('entries.show')->withEntry($entry);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$entry = Entry::findOrFail($id);
		if ($entry->user_id !== Auth::id()) {
			return redirectNotOwner();
		}

		return View::make('entries.edit')->withEntry($entry);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$entry = Entry::findOrFail($id);

		if ($entry->user_id !== Auth::id()) {
			return redirectNotOwner();
		}

		$rules = array(
			'title' => array('required', 'min:5', 'max:255'),
			'text' 	=> array('required', 'min:15')
		);

		$validator = Validator::make(
			Input::all(),
			$rules
		);

		if ($validator->fails()) {
			return Redirect::route('entries.edit', $id)->withErrors($validator)->withInput();
		}

		$entry->update(Input::all());

		$entry->save();

		return Redirect::route('entries.show', array('id' => $entry->id));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$entry = Entry::findOrFail($id);
		if ($entry->user_id !== Auth::id()) {
			return redirectNotOwner();
		}
		$entry->delete();

		return Redirect::route('entries.index');
	}

}
